﻿using System.Collections.Generic;

namespace Ex3.Models.Constants
{
    public class SimulatorConstants
    {
        public const string Lat = "Lat";
        public const string Lon = "Lon";
        public const string Rudder = "Rudder";
        public const string Throttle = "Throttle";

        // creating a dictionary that holds the vriable's paths and value.

        public static readonly Dictionary<string, string> FieldToNodePathDictionary = new Dictionary<string, string>()
        {
            { Lat , "/position/latitude-deg" },
            { Lon , "/position/longitude-deg"},
            { Rudder , "/controls/flight/rudder" },
            { Throttle ,"/controls/engines/current-engine/throttle" }
        };
    }
}