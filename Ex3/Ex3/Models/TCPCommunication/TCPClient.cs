﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Ex3.Models.TCPCommunication
{
    // Client class for the Commands Channel.
    public class TCPClient : IDisposable
    {
        private readonly IPAddress ipAddress;
        private readonly int port;
        private StreamWriter writer;
        private StreamReader reader;
        private NetworkStream stream;
        private TcpClient client;

        public bool IsConnected => this.client.Connected;
        
        public TCPClient(IPAddress ipAddress, int port)
        {
            this.ipAddress = ipAddress;
            this.port = port;
        }
         
        public void OpenClient()
        {
            IPEndPoint ep = new IPEndPoint(this.ipAddress, this.port);
            // connecting to the simulator.
            new Thread(() =>
            {
                while (true)
                {
                    try
                    {
                        client = new TcpClient();
                        client.Connect(ep);
                        break;
                    }
                    catch
                    {
                        Thread.Sleep(100);
                    }
                }
                stream = client.GetStream();
                writer = new StreamWriter(stream);
                reader = new StreamReader(stream);
            }).Start();
        }
            
        // releasing all allocated resources.
        public void Dispose()
        {
            writer?.Dispose();
            stream?.Dispose();
            client?.Close();
        }

        // sending the commands to the simulator.
        public bool Write(string command)
        {
            try
            {
                Console.WriteLine(command);
                writer?.Write(command);
                writer?.Flush();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //reading answers from the simulator.
        public string Read()
        {
            return reader.ReadLine();
        }

        
        
    }
}
