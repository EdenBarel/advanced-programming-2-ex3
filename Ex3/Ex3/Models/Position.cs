﻿namespace Ex3.Models
{
    public class Position
    {
        public float Lat { get; set; }
        public float Lon { get; set; }
    }
}