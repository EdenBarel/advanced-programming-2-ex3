﻿using System.ComponentModel.DataAnnotations;

namespace Ex3.Models
{
    public class DisplayModel
    {
        [Required]
        public int Rate { get; set; }
        [Required]
        public string ControllerName { get; set; }
        [Required]
        public int Period { get; set; }
    }
}