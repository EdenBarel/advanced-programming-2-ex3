﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Ex3.Models.Services
{
    public class FileReaderService
    {
        public static string file_name;

        public FileReaderService(string fileName)
        {
            file_name = fileName;
        }

        //reading all lines from the file, extracting the Lat and Lon valuse and adding them to the queue
        //that is defined in the Service class.
        public void ReadFile(HttpServerUtilityBase Server)
        {
            File.ReadAllLines(Server.MapPath("~/" + FileReaderService.file_name + ".csv")).ToList().ForEach(line=>
            {
                IEnumerable<float> values = line.Split(',').Select(str => float.Parse(str.Trim()));
                Position pos = new Position
                {
                    Lat = values.ElementAt(0),
                    Lon = values.ElementAt(1)
                };
                FlightSimulatorService.Instence.AddToQueue(pos);
            });
        }
    }
}