﻿using Ex3.Models.Constants;
using Ex3.Models.TCPCommunication;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ex3.Models.Services
{
    public class FlightSimulatorService
    {
        private float rudder;
        public float Rudder { get { return rudder; } set { if (rudder != value) { rudder = value; } } }
        private float throttle;
        public float Throttle { get { return throttle; } set { if (throttle != value) { throttle = value; } } }
        public Position CurrentPosition { get; set; }
        public TCPClient TcpClient { get; set; }
        private FileReaderService fileReder = null;
        private Queue<Position> qt = new Queue<Position>();

        public bool IsFromFile => TcpClient == null;

        //creating a FileReaderService object and stats the file reading.
        public void StartReadingFile(string fileName, HttpServerUtilityBase Server)
        {
            if (fileReder == null)
            {
                fileReder = new FileReaderService(fileName);
                fileReder.ReadFile(Server);
            }
        }

        //adding a Position object to the queue.
        public void AddToQueue(Position pos)
        {
            qt.Enqueue(pos);
        }

        //dequeue a Poition from the queue and updating the Lat and Lon values.
        public void UpdateFlightSimulatorService()
        {
            lock (qt)
            {
                //in case the queue is'nt empty - update Service class.
                if (qt.Any())
                {
                    Position pos = qt.Dequeue();
                    CurrentPosition.Lat = pos.Lat;
                    CurrentPosition.Lon = pos.Lon;
                }
                //else- initialize to Nan.
                else
                {
                    CurrentPosition.Lat = float.NaN;
                    CurrentPosition.Lon = float.NaN;
                }
            }
        }

        //constructor.
        private FlightSimulatorService()
        {
            CurrentPosition = new Position
            {
                Lat = float.NaN,
                Lon = float.NaN
            };
            this.TcpClient = null;
        }

        //defining the calss as a singletone class.
        private static FlightSimulatorService instance = null;
        public static FlightSimulatorService Instence
        {
            get
            {
                if (instance == null)
                {
                    instance = new FlightSimulatorService();
                }
                return instance;
            }
        }

        //closing the Client by calling to Dispose.
        public void CloseClient()
        {
            TcpClient?.Dispose();
            TcpClient = null;
        }

        //upating the service calss after the "get" request was sent.
        public void UpdateValues()
        {
            //if a tcpClient is exist and a conection with the Simulator succeeded - updete the Service.
            if (TcpClient != null && TcpClient.IsConnected)
            {
                this.Rudder = this.GetValueOf(SimulatorConstants.Rudder);
                this.Throttle = this.GetValueOf(SimulatorConstants.Throttle);
                this.CurrentPosition.Lat = this.GetValueOf(SimulatorConstants.Lat);
                this.CurrentPosition.Lon = this.GetValueOf(SimulatorConstants.Lon);
            }
        }

        //returning the actual value of a specific variable (Lat, Lon, Throttle, Rudder)
        public float GetValueOf(string fieldName)
        {
            float value;
            //geeitng the correct path by the name of the variable.
            string nodePath = SimulatorConstants.FieldToNodePathDictionary[fieldName];
            string ans;
            lock (TcpClient)
            {
                //sending a "get" request to the simulator.
                TcpClient.Write("get " + nodePath + "\r\n");
                //reading the answer we got from the simulator.
                ans = TcpClient.Read();
                value = float.Parse(ans.Split('\'')[1]);
            }
            return value;
        }
    }
}