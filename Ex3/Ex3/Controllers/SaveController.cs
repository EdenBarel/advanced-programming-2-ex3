﻿using Ex3.Models;
using Ex3.Models.Services;
using Ex3.Models.TCPCommunication;
using System;
using System.IO;
using System.Net;
using System.Web.Mvc;
using System.Xml.Serialization;


namespace Ex3.Controllers
{
    public class SaveController : Controller
    {
        // GET: save/127.0.0.1/5400/4/10/flight1
        public ActionResult Index(string ip, int port, int rate, int period, string file_name)
        {
            FlightSimulatorService.Instence.CloseClient();
            new FileReaderService(file_name);
            if (IPAddress.TryParse(ip, out IPAddress address))
            {
                FlightSimulatorService.Instence.TcpClient = new TCPClient(address, port);
                FlightSimulatorService.Instence.TcpClient.OpenClient();
            }
            return View("index", new DisplayModel() { Rate = rate, Period = period, ControllerName="Save" });
        }

        //converts an object to a xml.
        private string ToXml<T>(T obj)
        {
            //Initiate XML stuff
            StringWriter writer = new StringWriter();

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            xmlSerializer.Serialize(writer, obj);

            writer.Flush();
            return writer.ToString();
        }

        //this method is activated from the script one time every 4 seconds.
        //saving the current valuse of the Lat and Lon to the file from the simulator.
        [HttpPost]
        public string GetLatLon()
        {
            //updating the Service class with valuse that were recived from the simulator.
            FlightSimulatorService.Instence.UpdateValues();
            float lat = FlightSimulatorService.Instence.CurrentPosition.Lat;
            float lon = FlightSimulatorService.Instence.CurrentPosition.Lon;
            float throttle = FlightSimulatorService.Instence.Throttle;
            float rudder = FlightSimulatorService.Instence.Rudder;

            string line = lat + "," + lon + "," + throttle + "," + rudder + Environment.NewLine;
            //writing only actual values.
            if (!float.IsNaN(lat) && !float.IsNaN(lon))
            {
                lock (FileReaderService.file_name)
                {
                    System.IO.File.AppendAllText(Server.MapPath("~/" + FileReaderService.file_name + ".csv"), line);
                }
            }
            //turnning to xml so we could display the plains track during the saving action.
            return ToXml(FlightSimulatorService.Instence.CurrentPosition);
        }

    }
}