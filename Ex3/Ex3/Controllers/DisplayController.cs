﻿using Ex3.Models;
using Ex3.Models.Services;
using Ex3.Models.TCPCommunication;
using System.IO;
using System.Net;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace Ex3.Controllers
{
    public class DisplayController : Controller
    {
        // GET: display/127.0.0.1/5400/4
        public ActionResult StartFlight(string ip, int port, int rate)
        {
            FlightSimulatorService.Instence.CloseClient();
            if (IPAddress.TryParse(ip, out IPAddress address))
            {
                FlightSimulatorService.Instence.TcpClient = new TCPClient(address, port);
                FlightSimulatorService.Instence.TcpClient.OpenClient();
            }
            return View("index",new DisplayModel() { Rate = rate, ControllerName="Display", Period=-1 });
        }

        // GET: display/127.0.0.1/5400   or   display/flight1/4
        public ActionResult Index(string ip_fileName, int port_rate)
        {
            FlightSimulatorService.Instence.CloseClient();
            if (IPAddress.TryParse(ip_fileName, out IPAddress address))
            {
                string ip = ip_fileName;
                int port = port_rate;
                FlightSimulatorService.Instence.TcpClient = new TCPClient(address, port);
                FlightSimulatorService.Instence.TcpClient.OpenClient();
                return View("index", new DisplayModel() { Rate = 0, ControllerName = "Display", Period = -1 });
            }
            else
            {
                string fileName = ip_fileName;
                int rate = port_rate;
                FlightSimulatorService.Instence.StartReadingFile(ip_fileName, this.Server);
                return View("index",new DisplayModel() { Rate = rate, ControllerName = "Display", Period = -1 });
            }
        }

        //converts an object to a xml.
        private string ToXml<T>(T obj)
        {
            //Initiate XML stuff
            StringWriter writer =new StringWriter();

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            xmlSerializer.Serialize(writer, obj);
            
            writer.Flush();
            return writer.ToString();
        }

        //this method is activated from the script one time every 4 seconds.
        //getting the current valuse of the Lat and Lon from the file or the Simulator.
        [HttpPost]
        public string GetLatLon()
        {
            //if we should get the valuse from the file- then we update the service from out Psition's queue. 
            if (FlightSimulatorService.Instence.IsFromFile)
            {
                FlightSimulatorService.Instence.UpdateFlightSimulatorService();
            }
            //otherwise - we get the values from the simulator.
            FlightSimulatorService.Instence.UpdateValues();
            return ToXml(FlightSimulatorService.Instence.CurrentPosition);
        }
    }
}