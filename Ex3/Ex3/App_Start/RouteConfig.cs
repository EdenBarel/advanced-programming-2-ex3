﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Ex3
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Display With Three Parameters",
                url: "{controller}/{ip}/{port}/{rate}",
                defaults: new { controller = "Display", action = "StartFlight" }
            );

            routes.MapRoute(
                name: "Display With Two Parameters",
                url: "{controller}/{ip_fileName}/{port_rate}",
                defaults: new { controller = "Display", action = "Index" }
            );

            routes.MapRoute(
                name: "Save",
                url: "{controller}/{ip}/{port}/{rate}/{period}/{file_name}",
                defaults: new { controller = "Save", action = "Index" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}",
                defaults: new { controller = "Display", action = "Index"}
            );
        }
    }
}
